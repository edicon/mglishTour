/* global require, module */

var Angular2App = require('angular-cli/lib/broccoli/angular2-app');

module.exports = function(defaults) {
  return new Angular2App(defaults, {
    vendorNpmFiles: [
      'systemjs/dist/system-polyfills.js',
      'systemjs/dist/system.src.js',
      'zone.js/dist/**/*.+(js|js.map)',
      'es6-shim/es6-shim.js',
      'reflect-metadata/**/*.+(ts|js|js.map)',
      'rxjs/**/*.+(js|js.map)',
      // Angular
      '@angular/**/*.+(js|js.map)',
      // Angular Material2
      '@angular2-material/**/*.+(js|js.map)',
      // AngularFire2
      'angularfire2/**/*.+(js|js.map)',
      // Firebase v3.0
      'firebase/*.+(js|js.map)'
    ]
  });
};
