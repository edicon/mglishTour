import { MatDemoPage } from './app.po';

describe('mat-demo App', function() {
  let page: MatDemoPage;

  beforeEach(() => {
    page = new MatDemoPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('mat-demo works!');
  });
});
