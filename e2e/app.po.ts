export class MatDemoPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('mat-demo-app h1')).getText();
  }
}
