import { bootstrap } from '@angular/platform-browser-dynamic';
import { provide, enableProdMode } from '@angular/core';
// For md-icon
import { HTTP_PROVIDERS } from '@angular/http';
// New Forms API
import { disableDeprecatedForms, provideForms } from '@angular/forms';
import { MatDemoAppComponent, environment } from './app/';
import {
  FIREBASE_PROVIDERS,
  defaultFirebase,
  AngularFire,
  AuthMethods,
  AuthProviders,
  firebaseAuthConfig } from 'angularfire2';

if (environment.production) {
  enableProdMode();
}

bootstrap(MatDemoAppComponent, [
  disableDeprecatedForms(),
  provideForms(),
  HTTP_PROVIDERS,
  FIREBASE_PROVIDERS,
  // defaultFirebase('https://vivid-torch-3052.firebaseio.com/'),
   defaultFirebase({
    apiKey: "AIzaSyCT1e0MDRmnD_9juAp7VMWCLnJDiTJu7JY",
    authDomain: "vivid-torch-3052.firebaseapp.com",
    databaseURL: "https://vivid-torch-3052.firebaseio.com/",
    storageBucket: "vivid-torch-3052.appspot.com",
  }),
  firebaseAuthConfig({
    provider: AuthProviders.Password,
    method: AuthMethods.Password,
    // provider: AuthProviders.Google,
    // method: AuthMethods.Redirect
  })
]);
