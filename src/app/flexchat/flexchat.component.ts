import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { MessageComponent } from './message';
import { InputBarComponent } from './input-bar';


@Component({
  moduleId: module.id,
  selector: 'app-flexchat',
  templateUrl: 'flexchat.component.html',
  styleUrls: ['flexchat.component.css'],
    directives: [
    MessageComponent,
    InputBarComponent
  ]
})
export class FlexchatComponent implements OnInit {
  WorkAround: boolean = true;
  messages: FirebaseListObservable<any[]>;
  pushMessages: FirebaseListObservable<any[]>;
  limitSubject = new BehaviorSubject<number>(10);

  // Query Bug: push is a not function
  // Use workaround version of *.tar, see package.json
  constructor(af: AngularFire) {
    this.messages = af.database.list('/messages', {
      query: {
        limitToLast: this.limitSubject
      }
    });
    this.pushMessages = af.database.list('messages');
  }

  ngOnInit() {
    console.log('ngOninit: ');
    this.limitSubject.next(10);
  }

  addMessage(text) {
    if( this.WorkAround )
      this.pushMessages.push({text: text});
    else
      this.messages.push({ text: text });
  }

  changeLimit(limit) {
    this.limitSubject.next(parseInt(limit, 10));
  }
}
