import { Component, OnInit } from '@angular/core';
import {
  IS_PRERENDER,
  APP_SHELL_BUILD_PROVIDERS,
  APP_SHELL_RUNTIME_PROVIDERS,
  APP_SHELL_DIRECTIVES } from '@angular/app-shell';
import {
  Router, RouteConfig,
  ROUTER_DIRECTIVES,
  ROUTER_PROVIDERS } from '@angular/router-deprecated';
// import { Router, Routes, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router';

import {MdToolbar} from '@angular2-material/toolbar';
import { MdSpinner } from '@angular2-material/progress-circle';
import {MdButton} from '@angular2-material/button';
import {MdSidenav, MD_SIDENAV_DIRECTIVES} from '@angular2-material/sidenav';
import {MD_LIST_DIRECTIVES} from '@angular2-material/list';
import {MdIcon, MdIconRegistry} from '@angular2-material/icon';

import { ChapterComponent }  from './chapter/chapter.component';
import { ChaptersComponent } from './chapters/chapters.component';
import { Ngfire2Component }  from './ngfire2/ngfire2.component';
import { FlexchatComponent }  from './flexchat/flexchat.component';
import { AboutComponent }    from './about/about.component';
import { SideNavListComponent }  from './side-nav-list/side-nav-list.component';
// "angularfire2": "file:angularfire2-2.0.0-beta.0.tgz",


// Authentication in Angular 2
// https://medium.com/@blacksonic86/authentication-in-angular-2-958052c64492#.lkxtu0pip
@RouteConfig([
  {path: '/',          name: 'Root',     component: ChaptersComponent, useAsDefault: true },  // Default Router? '*', '/'
  {path: '/all-chapters',  name: 'AllChapters', component: ChaptersComponent },
  {path: '/chapter',   name: 'Chapter',  component: ChapterComponent },
  {path: '/chatting',  name: 'Chatting', component: FlexchatComponent },
  // {path: '/ng-fire2',  name: 'Chatting', component: Ngfire2Component },
  {path: '/about',     name: 'About',    component: AboutComponent },

])

@Component({
  moduleId: module.id,
  selector: 'mat-demo-app',
  /*
  template: `
    <md-sidenav-layout fullscreen (keydown.esc)="sidenav.close()">
    <md-sidenav #sidenav align="start" mode="over" opened=false>
      <md-nav-list >
        <img (click)="goMglish()" src="images/mglish_logo_banner_3go.png" />
        <side-nav-list [items]="items" (itemClicked)="sidenav.close()">
        </side-nav-list>
        <button class="close" md-raised-button (click)="sidenav.close()">Close</button>
        <p class="copyright">Copyright 2016 www.mglish.com</p>
      </md-nav-list>
    </md-sidenav>

    <md-toolbar color="primary">
      <button md-icon-button (click)="sidenav.open()">
        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
          <path fill="currentColor" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />
        </svg>
      </button>
      {{title}}
    </md-toolbar>
    <md-spinner *shellRender></md-spinner>

    <nav class="hidden">
      <ul>
        <li><a [routerLink]="['AllChapters']">Chapters</a></li>
        <li><a [routerLink]="['Chatting']">Chatting</a></li>
        <li><a [routerLink]="['About']">About</a></li>
      </ul>
    </nav>
    <router-outlet ></router-outlet>
    </md-sidenav-layout>
  `,
  styles: [ `
    md-sidenav-layout {
          background: rgba(0,0,0,0.08);
    }
    md-sidenav {
      width: 300px;
    }
    .md-list-item {
        display: flex;
        flex-direction: row;
        align-items: center;
    }
    nav {
        display: block;
    }
    nav.hidden {
        display: none;
    }
    nav.right {
        position: absolute;
        right: 0px;
        width: 300px;
        border: 1px solid #73AD21;
        padding: 10px;
    }
    nav ul li {
        display: inline;
    }
    button.close {
        margin:auto;
        display:block;
    }
    p.copyright {
        font-family: "Times New Roman", Times, serif;
        font-size: 10px;
        text-align: center;
        color: #ff9900;
    }
  `
  ],
  */
  templateUrl: 'mat-demo.component.html', // Not Yet App Shell
  styleUrls: ['mat-demo.component.css'],  // Not Yet App Shell
  providers: [
    Location,
    // IS_PRERENDER,
    APP_SHELL_BUILD_PROVIDERS,
    APP_SHELL_RUNTIME_PROVIDERS,
    ROUTER_PROVIDERS,
    MdSidenav,
    MdIconRegistry
  ],
  directives: [
    ROUTER_DIRECTIVES,
    MdToolbar,
    MdSpinner,
    MdButton,
    MD_SIDENAV_DIRECTIVES,
    MD_LIST_DIRECTIVES,
    MdIcon,
    APP_SHELL_DIRECTIVES,
    SideNavListComponent
  ]
})

export class MatDemoAppComponent implements OnInit {

  opened: boolean;
  title = 'Mglish: 쉽Go, 신나Go, 재미있Go';
  items: Object[] = [
    {
      id: 1,
      name: 'Mglish: Travel English',
      description: 'All Chapters of Travel English',
      icon: 'language'
    },
    {
      id: 2,
      name: 'Chatting',
      description: 'Share your idea...',
      icon: 'chat'
    },
    {
      id: 3,
      name: 'Login',
      description: 'Login for learning.',
      icon: 'account_circle'
    }
  ];

  constructor(
    private router: Router,
    private mdSidenav: MdSidenav ) {}

  ngOnInit() {
    // this.router.navigate(['/chapter']); // Angular2 RC1
    // this.router.navigate(['Chapter']);
  }

  // http://mean.expert/2016/05/21/angular-2-component-communication/
  navItemClicked( event ) {
    console.log('navItemClicked: ' + event );
  }

  goMglish() {
    console.log('Mglish Home ');
  }

  /*
  onClick( id: number ) {
    console.log('Before: ' + this.mdSidenav.opened);
    this.mdSidenav.close()
    // this.mdSidenav.toggle()
      .then(_ =>  {
        console.log('After: ' + this.mdSidenav.opened);
       })
      .catch( res => console.log('res: ' + res));

    console.log('id: ' + id );
    if( id === 1) {
      this.router.navigate(['Root']);
    } else {
      this.router.navigate(['About']);
    }
  }
  */
}