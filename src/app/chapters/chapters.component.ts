import { Component, OnInit } from '@angular/core';
// import { Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router';
import {
  Router,
  ROUTER_DIRECTIVES
  // ROUTER_PROVIDERS
} from '@angular/router-deprecated';

import { MdToolbar } from '@angular2-material/toolbar';
import { MdButton } from '@angular2-material/button';
import { MD_CARD_DIRECTIVES } from '@angular2-material/card';
import { MdIcon, MdIconRegistry } from '@angular2-material/icon';
import { AngularFire, FirebaseListObservable } from 'angularfire2';


import { Chapter }           from '../chapters/chapter';
import { ChapterService }    from '../chapter.service';

@Component({
  moduleId: module.id,
  selector: 'app-chapters',
  templateUrl: 'chapters.component.html',
  styleUrls: ['chapters.component.css'],
  providers: [
    // ROUTER_PROVIDERS,  <--- SHOULD BE DELETED, Mybe.. MUST Be in  Root Component
    MdIconRegistry,
    ChapterService
  ],
  directives: [
    ROUTER_DIRECTIVES,
    MD_CARD_DIRECTIVES,
    MdToolbar,
    MdButton,
    MdIcon,
    ]
})
export class ChaptersComponent implements OnInit {
  title = 'Mglish: Easy, Fun and..';
  baseUrl: string;
  CHAPTER_MAX_INDEX = 10;
  selectedChapter: Chapter;
  // chapters: FirebaseListObservable<any[]>;
  chapters: Object[];

  constructor(
    private router: Router,
    private chapterService: ChapterService ) {
  }

  ngOnInit() {
   this.getBaseUrl();
   this.getChapters();
    // this.title = this.chapters.name;
  }

  getBaseUrl() {
    this.chapterService.getBaseUrl()
      .subscribe( url => {
        this.baseUrl = url.$value;
        console.log(url.$value);
      });
  }

  getChapters() {
    // this.chapterService.getChapters().subscribe( chapters => { this.chapters = chapters; });
    this.chapters = this.chapterService.getChapters();
  }

  onSelect( chapter: Chapter ) {
    console.log('Chapter: ' + chapter.id );
    this.selectedChapter = chapter;
    this.title = chapter.name;
    this.goChapter( chapter.id );
  }

  goChapter( id: string ) {
    console.log('Go Chapter: ' + id );
    // this.router.navigateByUrl('http://localhost:4200/chapter');
    // window.history.back();
    this.router.navigate(['Chapter', {id: id, folder: this.selectedChapter.folder }]);
    // this.router.navigate(['/chapter', { id: id }]);
  }

  onMenu() {
    this.router.navigate(['Root']);
  }

  onSetting() {
    this.router.navigate(['About']);
  }
}
