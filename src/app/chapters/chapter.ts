export class Chapter {
  id: string;
  name: string;
  folder: string;
  description: string;
  image: string;
  page: [{}];
}
