import { Injectable } from '@angular/core';
import { AngularFire, FirebaseObjectObservable,FirebaseListObservable } from 'angularfire2';

import { Chapter }    from './chapters/chapter';

@Injectable()
export class ChapterService {

  baseUrl: FirebaseObjectObservable<any>;
  chapters: FirebaseListObservable<any[]>;
  chapterData: Array<Object>;
  selectedChapter: any;

  constructor( private af: AngularFire ) {
    this.baseUrl = af.database.object('/baseurl');
    this.chapters = af.database.list('/chapters');
    this.chapterData = new Array();
  }

  ngOnInit() {
    // No Call: this.chapterData = new Array();
  }

  getBaseUrl() {
    return this.baseUrl;
    // return 'https://googledrive.com/host/0By_uLlN6EPGGS0ZNakhUZDRyUHM';
  }

  getChapters() {
    // return this.chapters;
    this.chapters.forEach( chatper => {
      this.chapterData.length = 0;
      chatper.forEach( chap => {
        this.chapterData.push(chap);
      });
      this.chapterData.forEach( c => {
        console.log( 'chapter : ' + JSON.stringify(c) );
      });

    });
    return this.chapterData;
  }

  getChapter( chapId: string ) {
    /*
    this.chapters
      .map( chap => {
        console.log('getChapter: map: ' + chap);
        return chap;
      })
      .filter( chap  => {

        // chap.filter( c => c.val().id === chapId );
        console.log('getChapter: filter: ' + chap.keys );
        // return chapter.id === chapId;
        return true;
      })
      .subscribe( chap => {
        console.log('getChapter: subscribe: ' + chap );
        return chap;
      });
      */

    this.chapters.forEach( chatper => {
      chatper.forEach( chap => {
        if ( chapId === chap.id ) {
          console.log('chapId: ' +  chapId);
          this.selectedChapter = chap;
          // forEach는 Break가 없음.
          // return chap;
        }
      });
    });
    return this.selectedChapter;
  }

  findChapter( chapId: string ) {
    return this.chapters;
  }

  fChapter( chapId: string ) {
    this.chapters
    .do( chapter => {
        console.log( chapter );
    })
    .filter((chapter, i) => {
      console.log('filter: ' + chapter);
      if ( chapter[i] !== 'undefined') {
      return chapter[i].id === chapId;
      }
    })
    .subscribe( chapter => {
      console.log(chapter);
    });
  }
}
