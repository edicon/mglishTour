export class Page {
  no: number;
  image: string;
  audio: [
    { url: string }
  ];
}
