import { Component, OnInit } from '@angular/core';
import {
  Router,
  RouteParams
} from '@angular/router-deprecated';

// Material2
import {MdToolbar} from '@angular2-material/toolbar';
import {MdButton} from '@angular2-material/button';
import {MD_CARD_DIRECTIVES} from '@angular2-material/card';
import {MdIcon, MdIconRegistry} from '@angular2-material/icon';
// import {MD_SIDENAV_DIRECTIVES} from '@angular2-material/sidenav';
// import {MdProgressBar} from '@angular2-material/progress-bar';
// import {MD_INPUT_DIRECTIVES} from '@angular2-material/input';
// import {MD_LIST_DIRECTIVES} from '@angular2-material/list';

import { ChapterService }   from '../chapter.service';

@Component({
  moduleId: module.id,
  selector: 'app-chapter',
  templateUrl: 'chapter.component.html',
  styleUrls: ['chapter.component.css'],
  providers: [
    MdIconRegistry,
    ChapterService
  ],
  directives: [
    MD_CARD_DIRECTIVES,
    MdToolbar,
    MdButton,
    // MD_SIDENAV_DIRECTIVES,
    // MdCheckbox,
    // MdRadioButton,
    // MdSpinner,
    // MD_INPUT_DIRECTIVES,
    // MD_LIST_DIRECTIVES,
    // MdProgressBar,
    MdIcon,
    ]
})
export class ChapterComponent implements OnInit {
  title = 'Mglish: Travel English';
  baseUrl: string;
  // chapters: FirebaseListObservable<any[]>;
  chapter: any;

  CHAPTER_MAX_INDEX = 10;
  image: string;

  pageIndex: number;
  pageImages: Array<string>;
  totalPages: number;
  totalAudio: number;
  audioIndex: number;
  pageAudio: Array<string>;
  pagesAudio: any[][];
  nullAudio: boolean;

  audioPlay: any;
  audioError: string;
  selChapId: string;
  chapterFolder: string;

  constructor(
    params: RouteParams,
    private router: Router,
    private chapterService: ChapterService ) {
    this.selChapId = params.get('id');
    this.chapterFolder = params.get('folder');
    if ( this.selChapId === null ) {
      this.selChapId = '0';
    }
    console.log('id: ' + this.selChapId);

    this.pageAudio =  [];
    this.pageImages = [];
    this.pagesAudio = [];
  }

  ngOnInit() {
    this.pageIndex = 0;
    this.audioIndex = 0;
    // this.image = this.imageUrl + this.pageIndex + '.png';
    // Get Pages
    this.getBaseUrl();
    this.getChapterUrl();
    this.getPages();
    this.image = this.pageImages[this.pageIndex];
    // this.image = 'images/' + this.pageImages[this.pageIndex];
    this.CHAPTER_MAX_INDEX = this.pageImages.length - 1;
    console.log( 'pageImage : ' + this.image );
  }

  getBaseUrl() {
    this.chapterService.getBaseUrl()
      .subscribe( url => {
        this.baseUrl = url.$value;
        console.log(url.$value);
      });
  }

  getChapterUrl() {
    this.chapterService.findChapter( this.selChapId )
      .subscribe( chap => {
        for (var index = 0; index < chap.length; index++) {
          var chapter = chap[index];
          if ( chapter.id === this.selChapId ) {
            this.chapterFolder = chapter.folder;
            console.log('chapterFolder: ' + this.chapterFolder );
            break;
          }
        }
      });
  }

/*
  getPages() {
     this.chapterService.findChapter( this.selChapId )
      .subscribe( chap => {
        for (var index = 0; index < chap.length; index++) {
          var chapter = chap[index];
          if ( chapter.id === this.selChapId ) {
            this.chapterFolder = chapter.folder;
            console.log('chapterFolder: ' + this.chapterFolder );

            this.pageImages.length = 0;
            chapter.page.forEach( page => {
              this.pageImages.push(page.image);
              // Audio Per Page
              if ( this.pageIndex === parseInt(page.no)) {
                this.pageAudio.length = 0;
                page.audio.forEach( audio => {
                  this.pageAudio.push(audio.url);
                });
                this.pageImages.forEach( a => {
                  console.log( 'pageImage : ' + JSON.stringify(a) );
                });
                this.pageAudio.forEach( a => {
                  console.log( 'pageAudio : ' + JSON.stringify(a) );
                });
              }
            });
            if ( this.pageAudio.length === 0 ) {
              this.pageIndex = 0;
            }
            break;
          }
        }
      });
  }
  */

  getPages() {

    // this.chapterService.fChapter( this.selChapId );
    this.chapter = this.chapterService.getChapter( this.selChapId );

    if ( this.chapter != null ) {
      let pageIndex = 0;
      this.pageImages.length = 0;
      this.pagesAudio.length = 0;
      this.pagesAudio = [this.chapter.page.length];
      this.chapter.page.forEach( page => {
        this.pageImages.push(page.image);
        let index = 0;
        this.pageAudio.length = 0;
        this.pagesAudio[pageIndex] = [page.length];
        if ( typeof page.audio != 'undefined') {
          page.audio.forEach( audio => {
            this.pageAudio.push(audio.url);
            this.pagesAudio[pageIndex][index++] = audio.url;
          });
        }
        pageIndex++;
      });
      this.pageIndex = 0;
      this.totalPages = pageIndex;

      this.handleAudio();
      this.pagesAudio.forEach( audio => {
         audio.forEach( a => {
            console.log( 'pageAudio : ' + JSON.stringify(a) );
          });
      });
      console.log( 'pageImages : ' + this.pageImages );
    }
  }

  handleAudio() {

    this.image = this.pageImages[this.pageIndex];
    this.pageAudio = this.pagesAudio[this.pageIndex];
    // this.image = 'images/' + this.pageImages[this.pageIndex];

    this.nullAudio = false;
    this.audioIndex = 0;
    this.totalAudio = this.pageAudio.length;
    if ( typeof(this.pageAudio[0]) == 'undefined') {
      this.nullAudio = true;
      this.audioIndex = -1;
      this.totalAudio = 0;
    }
    this.pageAudio.forEach( a => {
      console.log( 'pageAudio : ' + JSON.stringify(a) );
    });
  }

  onPrev() {

    this.onStop();

    this.pageIndex = this.pageIndex - 1;
    if( this.pageIndex < 0 ) {
      this.pageIndex = 0;
    }

    this.handleAudio();
    console.log('Prev: ' + this.pageIndex);
  }

  onNext() {

    this.onStop();

    this.pageIndex = this.pageIndex + 1;
    if ( this.pageIndex > this.CHAPTER_MAX_INDEX ) {
      this.pageIndex = this.CHAPTER_MAX_INDEX;
    }

    this.handleAudio();
    console.log('Next: ' + this.pageIndex);
  }

  // Video Element: http://stackoverflow.com/questions/33770902/how-to-write-alternative-code-document-queryselector-in-angular-2-typescript/33771672#33771672
  // http://stackoverflow.com/questions/23231404/detecting-html5-audio-element-file-not-found-error?lq=1
  onPlay( audio: string ) {

    if ( typeof audio == 'undefined') {
      console.log('Play: ' + audio );
      return;
    }
    console.log('Play: ' + audio );

    // HSLEE: this.audioIndex = this.pageAudio.findIndex( a => { return a === audio; });
    this.audioIndex = this.pageAudio.indexOf( audio );

    try {
      if (typeof(this.audioPlay) != 'undefined' && this.audioPlay != null) {
        this.audioPlay.pause();
      }

      this.audioPlay = new Audio();
      this.audioPlay.addEventListener('error',
        function failed(e) {
           this.audioError = 'Audio Playback Error' + '(' + e.target.error.code + ')';
           console.log(this.audioError);
        }, true);
      this.audioPlay.src =
        this.baseUrl + '/'
        + this.chapterFolder + '/'
        + audio;
      // this.audioPlay.src = 'audio/' + audio; // this.pageAudio[this.audioIndex];
      this.audioPlay.load();
      this.audioPlay.play();
    } catch ( exception ) {
      this.audioError = 'Audio Error';
    }
  }

  onStop() {
    try {
      if (typeof(this.audioPlay) != 'undefined' && this.audioPlay != null) {
        this.audioPlay.pause();
      }
    } catch( e ) {
      this.audioError = 'Audio Error';
    }
  }

  showChapter() {
    console.log('Show Chapter: ' + this.pageIndex + ', image: ' + this.image );
  }

  onMenu() {
    this.router.navigate(['Root']);
  }

  onSetting() {
    this.router.navigate(['About']);
  }

  goBack() {
    window.history.back();
  }
}
