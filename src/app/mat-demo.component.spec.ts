import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { MatDemoAppComponent } from '../app/mat-demo.component';

beforeEachProviders(() => [MatDemoAppComponent]);

describe('App: MatDemo', () => {
  it('should create the app',
      inject([MatDemoAppComponent], (app: MatDemoAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'mat-demo works!\'',
      inject([MatDemoAppComponent], (app: MatDemoAppComponent) => {
    expect(app.title).toEqual('mat-demo works!');
  }));
});
