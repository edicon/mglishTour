import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import 'rxjs/add/operator/map';

@Component({
  moduleId: module.id,
  selector: 'app-ngfire2',
  templateUrl: 'ngfire2.component.html',
  styleUrls: ['ngfire2.component.css']
})
export class Ngfire2Component implements OnInit {
  chapters: FirebaseListObservable<any[]>;
  chapter: Object;

  constructor(af: AngularFire) {
    this.chapters = af.database.list('/chapters');
  }

  ngOnInit() {
    console.log('chapters: ' + this.chapters);
    
    this.chapters.forEach( chapter => {
      console.log( JSON.stringify( chapter ) + '\n');

      chapter.forEach( chap => {
        // Chapter
        // if( chap.$key === '1' ) {
        if ( chap.id === '1' ) {
          console.log( 'Chapter:Key: ' + chap.$key + ', page: ' + chap.page );
          chap.page.forEach( page => {
            page.audio.forEach( audio => {
              console.log( 'Audio : ' + audio.url );
            });
          });
        }
      });
      // console.log('$key: ' + ch['$key'] + ch );
    });
  }
}
