import {
  beforeEachProviders,
  it,
  describe,
  expect,
  inject
} from '@angular/core/testing';
import { ChapterService } from './chapter.service';

describe('Chapter Service', () => {
  beforeEachProviders(() => [ChapterService]);

  it('should ...',
      inject([ChapterService], (service: ChapterService) => {
    expect(service).toBeTruthy();
  }));
});
