import { Input, Output, EventEmitter,Component, OnInit } from '@angular/core';
import { Router } from '@angular/router-deprecated';

import {MdToolbar} from '@angular2-material/toolbar';
import {MdButton} from '@angular2-material/button';
import {MdSidenav, MD_SIDENAV_DIRECTIVES} from '@angular2-material/sidenav';
import {MD_LIST_DIRECTIVES} from '@angular2-material/list';
import {MdIcon, MdIconRegistry} from '@angular2-material/icon';

@Component({
  moduleId: module.id,
  selector: 'side-nav-list',
  templateUrl: 'side-nav-list.component.html',
  providers: [
    MdSidenav,
    MdIconRegistry
  ],
  directives: [
    MdButton,
    MD_LIST_DIRECTIVES,
    MdIcon
  ],
  styleUrls: ['side-nav-list.component.css']
})
export class SideNavListComponent implements OnInit {

  @Input()  items;
  @Output() itemClicked = new EventEmitter();

  constructor( private router: Router ) {}

  ngOnInit() {
    console.log('side-nav-list: ' + this.items );
  }

  onClick( id: number ) {
    console.log('onClick: ' + id )

    if( id === 1) {
      this.router.navigate(['Root']);
    } else if ( id === 2 ) {
      this.router.navigate(['Chatting']);
    } else if ( id === 3 ) {
      this.router.navigate(['About']);
    }
    // Send @Output Event
    this.itemClicked.emit(
      { value: id });
  }
}
