import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router-deprecated';
import {MdToolbar} from '@angular2-material/toolbar';
import {MdButton} from '@angular2-material/button';
import {MD_CARD_DIRECTIVES} from '@angular2-material/card';
import {MdIcon, MdIconRegistry} from '@angular2-material/icon';

import { AngularFire, FirebaseObjectObservable, FirebaseListObservable,
  AuthMethods,
  AuthProviders  } from 'angularfire2'

@Component({
  moduleId: module.id,
  selector: 'app-about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.css'],
    providers: [
    MdIconRegistry
  ],
  directives: [
    MD_CARD_DIRECTIVES,
    MdToolbar,
    MdButton,
    // MD_SIDENAV_DIRECTIVES,
    // MdCheckbox,
    // MdRadioButton,
    // MdSpinner,
    // MD_INPUT_DIRECTIVES,
    // MD_LIST_DIRECTIVES,
    // MdProgressBar,
    MdIcon,
    ]
})
export class AboutComponent implements OnInit {

  constructor(
     private router: Router,
     public af: AngularFire
  ) { }

  ngOnInit() {
  }

  anoLogin() {
    // this.af.auth.login();
    // Anonymous
    this.af.auth.login({
      provider: AuthProviders.Anonymous,
      method: AuthMethods.Anonymous
    })
    .then(_ => console.log('Login: OK'))
    .catch( e => console.log('Login: Failed'));
  }
   emailLogin() {

    this.af.auth.login({
      email: 'hslee.edicon@gmail.com', password: '1234' })
      .then(_ => console.log('Email Login: OK'))
      .catch( e => console.log('Email Login: Failed'));
  }

  // Twitter
  twittLogin() {
    this.af.auth.login({
      provider: AuthProviders.Twitter,
      method: AuthMethods.Popup,
    })
    .then(_ => console.log('Twitter Login: OK'))
    .catch( e => console.log('Twitter Login: Failed'));
  }

  goMglish() {
    this.router.navigate(['Root']);
  }

  gotoChapters() {
     this.router.navigate(['AllChapters']);
  }
}
